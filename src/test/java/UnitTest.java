import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    @DisplayName("Einfache Plusrechnung")
    void testAddition() {
        assertEquals(9, calculator.plusRechnen(4, 5), "Test hat geklappt");
        assertEquals(-1, calculator.plusRechnen(4, -5), "Test hat geklappt");
        assertEquals(-9, calculator.plusRechnen(-4, -5), "Test hat geklappt");
    }

    @Test
    @DisplayName("Einfache Minusrechnung")
    void testSubtraction() {
        assertEquals(4, calculator.minusRechnen(9, 5), "Test hat geklappt");
        assertEquals(-4, calculator.minusRechnen(5, 9), "Test hat geklappt");
        assertEquals(9, calculator.minusRechnen(5, -4), "Test hat geklappt");
        assertEquals(-1, calculator.minusRechnen(-5, -4), "Test hat geklappt");
    }

    @Test
    @DisplayName("Einfache Malrechnung")
    void testMultiply() {
        assertEquals(20, calculator.malRechnen(4, 5), "Test hat geklappt");
        assertEquals(-20, calculator.malRechnen(-4, 5), "Test hat geklappt");
        assertEquals(20, calculator.malRechnen(-4, -5), "Test hat geklappt");
    }

    @Test
    @DisplayName("Einfache Geteiltrechnung")
    void testDivision() {
        assertEquals(5, calculator.geteiltRechnen(10, 2), "Test hat geklappt");
        assertEquals(-5, calculator.geteiltRechnen(10, -2), "Test hat geklappt");
        assertEquals(5, calculator.geteiltRechnen(-10, -2), "Test hat geklappt");
    }

    @Test
    public void DurchNullTeilen() {
        boolean expectedExeption = false;

        try {
            int divided = calculator.geteiltRechnen(1, 0);
        } catch (ArithmeticException ae) {
            expectedExeption = true;
        }
        assertTrue(expectedExeption);
    }
}
