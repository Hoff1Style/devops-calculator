import java.util.Scanner;

/**
 * The type Main.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        calculating();
    }

    /**
     * Taschenrechner Funktion.
     */
    public static void calculating() {

        int ersteZahl, zweiteZahl;
        String rechenoperation;
        Scanner scanner = new Scanner(System.in);

        // Eingabe erste Zahl
        do {
            try {
                System.out.println("Gebe die erste Zahl ein: ");
                ersteZahl = scanner.nextInt();
                break;
            } catch (Exception e) {
                System.out.println("Bitte nur Zahlen eingeben!");
                scanner.nextLine();
            }

        } while (true);

        // Eingabe zweite Zahl
        do {
            try {
                System.out.println("Gebe die zweite Zahl ein: ");
                zweiteZahl = scanner.nextInt();
                break;
            } catch (Exception e) {
                System.out.println("Bitte nur Zahlen eingeben!");
                scanner.nextLine();
            }

        } while (true);

        // Eingabe Operator
        do {
            scanner.nextLine();
            System.out.println("Gebe die Rechenoperation ein [+ - * /]: ");
            rechenoperation = scanner.next();
            System.out.println(rechenoperation);
        } while (!rechenoperation.equals("+") && !rechenoperation.equals("-") && !rechenoperation.equals("*") && !rechenoperation.equals("/"));



        Calculator calculator = new Calculator();

        // Ausgabe Ergebnis
        if ("+".equals(rechenoperation)) {
            System.out.println("Ergebnis: " + calculator.plusRechnen(ersteZahl, zweiteZahl));
        } else if ("-".equals(rechenoperation)) {
            System.out.println("Ergebnis: " + calculator.minusRechnen(ersteZahl, zweiteZahl));
        } else if ("*".equals(rechenoperation)) {
            System.out.println("Ergebnis: " + calculator.malRechnen(ersteZahl, zweiteZahl));
        } else if ("/".equals(rechenoperation)) {
            System.out.println("Ergebnis: " + calculator.geteiltRechnen(ersteZahl, zweiteZahl));
        } else {
            System.out.println("Falsche Rechenoperation");
        }
    }
}
