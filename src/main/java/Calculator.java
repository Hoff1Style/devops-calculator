/**
 * Die Klasse Calculator.
 */
public class Calculator {

    /**
     * Einfache Addition
     *
     * @param a Beliebiger Integer
     * @param b Beliebiger Integer
     * @return Summe als Integer
     */
    public int plusRechnen(int a, int b) {
        return a + b;
    }

    /**
     * Einfache Subtraktion
     *
     * @param a Beliebiger Integer
     * @param b Beliebiger Integer
     * @return Differenz als Integer
     */
    public int minusRechnen(int a, int b) {
        return a - b;
    }

    /**
     * Einfache Multiplikation
     *
     * @param a Beliebiger Integer
     * @param b Beliebiger Integer
     * @return Produkt als Integer
     */
    public int malRechnen(int a, int b) {
        return a * b;
    }

    /**
     * Einfache Division
     *
     * @param a Beliebiger Integer
     * @param b Beliebiger Integer
     * @return Quotient als Integer
     */
    public int geteiltRechnen(int a, int b) {
        return a / b;
    }
}
